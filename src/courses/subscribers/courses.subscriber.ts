import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';
import { Course } from '../course.entity';

@EventSubscriber()
export class CoursesSubscriber implements EntitySubscriberInterface<Course> {
  constructor(connection: Connection) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return Course;
  }

  beforeInsert(event: InsertEvent<Course>) {
    console.log(`BEFORE COURSE INSERTED: `, event.entity);
  }
}
