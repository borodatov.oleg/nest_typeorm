import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';
import { Course } from './course.entity';
import { CoursesService } from './services';
import { PageArgs, CourseDto } from 'src/dto';

const pubSub = new PubSub();

@Resolver(of => Course)
export class CoursesResolver {
  constructor(private readonly coursesService: CoursesService) {}

  @Query(returns => Course)
  async course(@Args('id') id: string): Promise<Course> {
    const course = await this.coursesService.findOne(id);
    if (!course) {
      throw new NotFoundException(id);
    }
    return course;
  }

  @Query(returns => [Course])
  courses(@Args() pageArgs: PageArgs): Promise<Course[]> {
    return this.coursesService.findAll(pageArgs);
  }

  @Mutation(returns => Course)
  async addCourse( @Args('newCourseData') newCourseData: CourseDto ): Promise<Course> {
    const course = await this.coursesService.create(newCourseData);
    pubSub.publish('courseAdded', { CourseAdded: course });
    return course;
  }

  @Mutation(returns => Boolean)
  async removeCourse(@Args('id') id: string) {
    return this.coursesService.remove(id);
  }

  @Subscription(returns => Course)
  courseAdded() {
    return pubSub.asyncIterator('courseAdded');
  }
}
