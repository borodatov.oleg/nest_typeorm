import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { CourseDto, PageArgs } from 'src/dto';
import { Course } from '../course.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
    private readonly connection: Connection,
  ) {}

  create(courseDto: CourseDto): Promise<Course> {
    const course = new Course();
    return this.coursesRepository.save({...course, ...courseDto});
  }

  async update(courseDto: CourseDto): Promise<Course> {
    const course = await this.coursesRepository.findOne(courseDto.id);
    return this.coursesRepository.save({...course, ...courseDto});
  }

  findAll(pageArgs: PageArgs): Promise<Course[]> {
    return this.coursesRepository.find({ relations: ['users'], skip: pageArgs.skip, take: pageArgs.take });
  }

  findOne(id: string): Promise<Course> {
    return this.coursesRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.coursesRepository.delete(id);
  }

  async createMany(Courses: Course[]) {
    await this.connection.transaction(async manager => {
      Courses.forEach(async course => {
        await manager.save(course);
      });
    });
  }
}
