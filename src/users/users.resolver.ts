import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';
import { User } from './user.entity';
import { UsersService } from './services';
import { PageArgs, UserDto } from 'src/dto';

const pubSub = new PubSub();

@Resolver(of => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(returns => User)
  async user(@Args('id') id: string): Promise<User> {
    const User = await this.usersService.findOne(id);
    if (!User) {
      throw new NotFoundException(id);
    }
    return User;
  }

  @Query(returns => [User])
  users(@Args() pageArgs: PageArgs): Promise<User[]> {
    return this.usersService.findAll(pageArgs);
  }

  @Mutation(returns => User)
  async addUser( @Args('newUserData') newUserData: UserDto ): Promise<User> {
    const User = await this.usersService.create(newUserData);
    pubSub.publish('UserAdded', { UserAdded: User });
    return User;
  }

  @Mutation(returns => Boolean)
  async removeUser(@Args('id') id: string) {
    return this.usersService.remove(id);
  }

  @Subscription(returns => User)
  userAdded() {
    return pubSub.asyncIterator('UserAdded');
  }
}
