import { Module } from '@nestjs/common';
import { CoursesService } from './services';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoursesSubscriber } from './subscribers';
import { Course } from './course.entity';
import { CoursesResolver } from './courses.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([Course])],
  providers: [CoursesService, CoursesSubscriber, CoursesResolver],
})
export class CoursesModule {}
