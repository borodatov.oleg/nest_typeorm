import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { UserDto, PageArgs } from 'src/dto';
import { User } from '../user.entity';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly connection: Connection,
    @InjectQueue('exam') private readonly audioQueue: Queue
  ) {}

  async create(userDto: UserDto): Promise<User> {
    const user = new User();
    const addedUser = await this.usersRepository.save({...user, ...userDto});
    await this.passExam(addedUser, ['uno', 'dos', 'tres', 'komsi komsi komsa']);
    return addedUser;
  }

  async update(userDto: UserDto): Promise<User> {
    const user = await this.usersRepository.findOne(userDto.id);
    return this.usersRepository.save({...user, ...userDto});
  }

  findAll(pageArgs: PageArgs): Promise<User[]> {
    return this.usersRepository.find({ relations: ['courses'], skip: pageArgs.skip, take: pageArgs.take });
  }

  findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  async createMany(users: User[]) {
    await this.connection.transaction(async manager => {
      users.forEach(async user => {
        await manager.save(user);
      });
    });
  }

  async passExam(user: User, answers: string[]) {
    await this.audioQueue.add({
      userId: user.id,
      answers: answers,
    });
  }
}
