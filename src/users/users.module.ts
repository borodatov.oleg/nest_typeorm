import { Module } from '@nestjs/common';
import { UsersService } from './services';
import { UserSubscriber } from './subscribers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UsersResolver } from './users.resolver';
import { ExamProcessor } from './exam.processor';
import { BullModule } from '@nestjs/bull';
import { configService } from 'src/config/config.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    BullModule.registerQueue({
      name: 'exam',
      redis: {
        host: configService.getValue('QUEUE_HOST'),
        port: +configService.getValue('QUEUE_PORT'),
      },
    }),
  ],
  providers: [UsersService, UserSubscriber, UsersResolver, ExamProcessor],
})
export class UsersModule {}
