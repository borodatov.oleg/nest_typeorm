import { Entity, Column, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { User } from 'src/users/user.entity';

@Entity()
@ObjectType()
export class Course {
  @PrimaryGeneratedColumn()
  @Field(type => ID)
  id: number;

  @Column()
  @Field({ nullable: false })
  name: string;

  @ManyToMany(type => User, user => user.courses)
  @Field(type => [User])
  users: User[];
}