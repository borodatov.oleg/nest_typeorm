import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Course } from 'src/courses/course.entity';

@Entity()
@ObjectType()
export class User {
  @PrimaryGeneratedColumn()
  @Field(type => ID)
  id: number;

  @Column()
  @Field({ nullable: false })
  firstName: string;

  @Column()
  @Field({ nullable: false })
  lastName: string;

  @Column({ default: true })
  @Field({ nullable: true })
  isActive: boolean;

  @ManyToMany(type => Course, course => course.users)
  @JoinTable()
  @Field(type => [Course], { nullable: true })
  courses: Course[];
}