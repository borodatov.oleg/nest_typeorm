import { MaxLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class CourseDto {
  @Field({ nullable: true })
  id: number;

  @Field()
  @MaxLength(30)
  name: string;
}
