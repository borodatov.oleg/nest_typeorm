import { CourseDto } from './course.dto';
import { IsOptional, Length, MaxLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class UserDto {
  @Field({ nullable: true })
  id: number;

  @Field()
  @MaxLength(50)
  firstName: string;

  @Field()
  @MaxLength(40)
  lastName: string;

  @Field(type => [CourseDto], { nullable: true })
  courses: CourseDto[];
}
