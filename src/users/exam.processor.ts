import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { UsersService } from './services';

@Processor('exam')
export class ExamProcessor {
  private readonly logger = new Logger(ExamProcessor.name);

  constructor(private userService: UsersService) {
  }

  @Process()
  async handleTranscode(job: Job) {
    const user = await this.userService.findOne(job.data.userId);
    this.logger.debug(`Processing ${user.firstName} ${user.lastName}`);
    this.logger.debug(job.data.answers);
    this.logger.debug('Processing completed');
  }
}